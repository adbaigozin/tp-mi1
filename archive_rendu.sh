#!/usr/bin/bash

clear_line() {
  printf '\r\033[2K'
}

move_cursor_up() {
  printf '\033[1A'
}

fgs=($(seq 30 37) 39 $(seq 90 97))
bgs=($(seq 40 47) 49 $(seq 100 107))
attrs=(1 2 4 5 7 8)

set_font () {
	local return
	case "${1,,}" in
		#FOREGROUND COLOR
		39|defaultfg) printf "\e[39m";;
		30|blackfg|black) printf "\e[30m";;
		31|redfg|red) printf "\e[31m";;
		32|greenfg|green) printf "\e[32m";;
		33|yellowfg|yellow) printf "\e[33m";;
		34|bluefg|blue) printf "\e[34m";;
		35|magentafg|magenta) printf "\e[35m";;
		36|cyanfg|cyan) printf "\e[36m";;
		37|lightgrayfg|lightgray|"light gray") printf "\e[37m";;
		90|darkgrayfg|darkgray|"dark gray") printf "\e[90m";;
		91|lightredfg|lightred|"light red") printf "\e[91m";;
		92|lightgreenfg|lightgreen|"light green") printf "\e[92m";;
		93|lightyellowfg|lightyellow|"light yellow") printf "\e[93m";;
		94|lightbluefg|lightblue|"light blue fg") printf "\e[94m";;
		95|lightmagentafg|lightmagenta|"light magenta fg") printf "\e[95m";;
		96|lightcyanfg|lightcyan|"light cyan fg") printf "\e[96m";;
		97|whitefg|white) printf "\e[97m";;
		#BACKGROUND COLOR
		49|defaultbg) printf "\e[49m";;
		40|blackbg) printf "\e[40m";;
		41|redbg) printf "\e[41m";;
		42|greenbg) printf "\e[42m";;
		43|yellowbg) printf "\e[43m";;
		44|bluebg) printf "\e[44m";;
		45|magentabg) printf "\e[45m";;
		46|cyanbg) printf "\e[46m";;
		47|ligthgraybg|"light gray bg") printf "\e[47m";;
		100|darkgraybg|"dark gray bg") printf "\e[100m";;
		101|ligthredbg|"light red bg") printf "\e[101m";;
		102|ligthgreenbg|"light green bg") printf "\e[102m";;
		103|ligthyellowbg|"light yellow bg") printf "\e[103m";;
		104|ligthbluebg|"light blue bg") printf "\e[104m";;
		105|ligthmagentabg|"light magenta bg") printf "\e[105m";;
		106|ligthcyanbg|"light cyan bg") printf "\e[106m";;
		107|whitebg) printf "\e[107m";;
		#ATTRIBUTES SETTERS
		1|bold|bright) printf "\e[1m";;
		2|dim) printf "\e[2m";;
		4|underlined) printf "\e[4m";;
		5|blink) printf "\e[5m";;
		7|reverse|swap) printf "\e[7m";;
		8|hidden) printf "\e[8m";;
		#ATTRIBUTES UNSETTERS
		0|reset|default) printf "\e[0m";;
		21|23|21,23|unbold|unbright) printf "\e[21m\e[23m";;
		22|undim) printf "\e[22m";;
		24|ununderlined) printf "\e[24m";;
		25|unblink) printf "\e[25m";;
		27|unreverse|unswap) printf "\e[27m";;
		28|unhidden) printf "\e[28m";;
		#SPECIAL
		movecursorup) printf '\033[1A';;
		clearline) printf '\r\033[2K';;
		#RANDOM
		randomfg)
			set_font ${fgs[$(( $RANDOM % ${#fgs[@]}))]};;
		randombg)
			set_font ${bgs[$(( $RANDOM % ${#bgs[@]}))]};;
		randomattr)
			set_font ${attrs[$(( $RANDOM % ${#attrs[@]}))]};;
		random)
			set_font randomfg randombg randomattr;;
		#AS UNIQUE AS POSSIBLE FROM INT (GIVEN AS 'n<N>')
		n*)
			n=$(( ${1#n} + 0 ))
			set_font ${fgs[$(( $n % ${#fgs[@]} ))]}
			n=$(( $n / ${#fgs[@]} ))
			set_font ${bgs[$(( $n % ${#bgs[@]} ))]}
			n=$(( $n / ${#bgs[@]} ))
			set_font ${attrs[$(( $n % ${#attrs[@]} ))]}
			;;
		#MANUAL OR FAIL
		*)
			#missing numbers are: 3, 6, 9, 10:21, 23, 26, 29, 38, 48, 50:90, 98:100, 108: (seem to have no effect)
			if [[ "$1" =~ '[0-9]\+' ]]
			then
				printf "\e[$1m"
			else
				#RESET ALL
				printf "\e[39m\e[49m\e[0m"
				[[ -n "$1" ]] && return -1
			fi;;
	esac
	return=$?
	[ $return -ne 0 ] && return $return
	shift
	if [[ -n "$1" ]]
	then
		set_font $@
	fi
}

## error and warning messages
test -n "$COLUMNS" || COLUMNS=$(tput cols) || COLUMNS=80
error () {
	{
		echo -n "$(set_font redfg bold)Erreur: "
		flag=false
		while test $# -gt 0
		do
			$flag && echo -n "$(set_font redfg bold)" || flag=true
			echo "$1$(set_font)"
			shift
		done
	} | fold -sw $COLUMNS | sed -e "s/^/$indent  /g" | sed -e '2,$s/^/  /g' >&2
}
warning () {
	{
		echo -n "$(set_font yellowfg)Avertissement: "
		flag=false
		while test $# -gt 0
		do
			$flag && echo -n "$(set_font yellowfg)" || flag=true
			echo "$1$(set_font)"
			shift
		done
	} | fold -sw $COLUMNS | sed -e "s/^/$indent  /g" | sed -e '2,$s/^/  /g' >&2
}
valid () {
	{
		echo -n "$(set_font greenfg)Félicitations: "
		flag=false
		while test $# -gt 0
		do
			$flag && echo -n "$(set_font greenfg)" || flag=true
			echo "$1$(set_font)"
			shift
		done
	} | fold -sw $COLUMNS | sed -e "s/^/$indent  /g" | sed -e '2,$s/^/  /g'
}

## checkers
repocheck () {
	echo "Vérification du dépôt... "
	indent='  '

	if git status 2>/dev/null >&2
	then
		mkdir -p "$output"
		rm -fr "$output"
		git clone ./ "$output" 2>/dev/null >&2
	else
		error "le script $0 doit être appelé à l'intérieur du dépôt git."
		exit 0
	fi

	echo "faite."
}

ex1check () {
	echo "Vérification de l'exercice 1... "
	indent='  '

	#Testing identity
	if name="$(git config --local user.name)"
	then
		echo "$name" | grep -q '^[A-Z][-A-Z]\+ [A-Z].*$' \
			|| warning "Le nom '$(set_font dim)$name$(set_font undim)' indiqué (Exo1.Q3) ne semble pas être au format 'NOM Prénom'."
	else
		name="$(git config --global user.name)" \
			&& warning "Le nom '$(set_font dim)$name$(set_font undim)' indiqué (Exo1.Q3) n'a pas été défini localement (n'utilisez pas l'option '--global' lors de votre git config)." \
			|| warning "Le nom n'a pas été configuré dans le dépôt (Exo1.Q3)."
	fi
	if email="$(git config --local user.email)"
	then
		echo "$email" | grep -q '^[^ ]*@etu.uca.fr$' \
			|| warning "L'adresse mail '$(set_font dim)$email$(set_font undim)' indiquée (Exo1.Q3) ne semble pas être une adresse mail étudiante (...@etu.uca.fr)'."
	else
		email="$(git config --global user.email)" \
			&& warning "L'adresse mail '$(set_font dim)$email$(set_font undim)' indiquée (Exo1.Q3) n'a pas été définie localement (n'utilisez pas l'option '--global' lors de votre git config)." \
			|| warning "L'adresse mail n'a pas été configurée dans le dépôt (Exo1.Q3)."
	fi
	#Testing student number
	if test -f "./user.id"
	then
		#TODO: review warnings from here!!!
		test "$(cat "./user.id" | wc -l)" -ne 1 \
			&& warning "Le fichier './user.id' devrait contenir exactement 1 ligne ($(cat "./user.id" | wc -l) trouvée)." \
			|| grep -qv '^[0-9]\{8\}$' "./user.id" \
			&& warning "Le fichier './user.id' ne semble pas contenir un numéro d'étudiant valide '$(set_font dim)$(cat "./user.id")$(set_font undim)'."
		#Testing commit
		if git log sujet..HEAD 2>/dev/null >&2
		then
			#Testing commit index
			if git status --short -- "user.id" | grep -q '^??'
			then
				warning "Le fichier './user.id' n'est pas tracé par git (Ex1.Q5)" \
					"Vous pouvez utiliser la commande $(set_font white)\$ git command --amend$(set_font yellow) pour corriger votre commit."
				cp "./user.id" -ft "$output/"
				git -C "$output" add "user.id"
				git -C "$output" commit -m "[script]: add user.id  ---  $(git status --short -- user.id)" >/dev/null
			elif test -n "$(git status --short -- "user.id")"
			then
				warning "Le fichier './user.id' est bien tracé par git mais a été modifié depuis la dernière version (et donc depuis la première version) (Ex1.Q5)."
						"Vous pouvez utiliser la commande $(set_font white)\$ git command --amend$(set_font yellow) pour corriger votre commit."
				cp "./user.id" -ft "$output/"
				git -C "$output" add "user.id"
				git -C "$output" commit -m "[script]: add user.id  ---  $(git status --short -- user.id)" >/dev/null
			elif ! git show --name-only --oneline $(git log --oneline sujet..HEAD | tail -1 | cut -d' ' -f1) | grep -q '^user.id$'
			then
				warning "Le fichier './user.id' est bien tracé par git mais n'a pas été ajouté lors de la première version (Ex1.Q5)." \
					"Vous pouvez utiliser la commande $(set_font white)\$ git command --amend$(set_font yellow) pour corriger votre commit."
			fi
			#Testing commit message
			git log --format=format':%s' sujet..HEAD | tail -1 | grep -q '^spécification du numéro d'"'"'étudiant$' \
				|| warning "Le message du premier commit devait être exactement '$(set_font dim)spécification du numéro d'étudiant$(set_font undim)' (Ex1.Q5), mais '$(set_font dim)$(git log sujet..HEAD | tail -1 | sed 's/^    //')$(set_font undim)' a été donné."\
					"Vous pouvez utiliser la commande $(set_font white)\$ git command --amend$(set_font yellow) pour corriger votre commit."
		else
			warning "Vous n'avez créé aucune version (Ex1.Q5)."
			cp "user.id" -ft "$output"
			git -C "$output" add "user.id"
			git -C "$output" commit -m "[script]: add user.id  --- $(git status --short -- user.id)" >/dev/null
		fi
	fi

	echo "faite."
}
ex2check () {
	test -z "$(git diff sujet -- "exo2.tex" | head -1)" \
		&& warning "Exercice 2 non fait (aucun changement détecté sur le fichier 'exo2.tex')..." \
		&& return 0

	echo "Vérification de l'exercice 2... "
	indent='  '

	#Test compilation
	cp "exo2.tex" -ft "$tmpdir/"
	pushd "$tmpdir" >/dev/null
	pdflatex -interaction nonstopmode "exo2.tex" >/dev/null 2>&1 </dev/null \
		|| warning "Le fichier 'exo2.tex' ne compile pas correctement."
	popd >/dev/null

	#Test header
	sed -n '/^[^%]*\\begin{document}/,/^[^%]*\\end{document}/p' "exo2.tex" | grep -q '^[^%]*\\LaTeX' \
		|| warning "Faites-vous bien afficher le logo LaTeX (Ex2.Q2)?"

	#Test strong
	sed -n '/^[^%]*\\begin{document}/,/^[^%]*\\end{document}/p' "exo2.tex" | grep -q '^[^%]*\\strong' \
		|| warning "Avez-vous bien pensé à utiliser la commande $(set_font dim)\\strong$(set_font undim) que vous avez définie (Ex2.Q4)?"

	#Test commit
	git status --short -- "exo2.tex" | read \
		&& warning "Le contenu actuel du fichier 'exo2.tex' ne semble pas avoir été pris en compte dans la dernière version (Ex2.Q5)." \
		&& cp -f "exo2.tex" "$output/exo2.uncommitted.tex" \
		&& git -C "$output" add "exo2.uncommitted.tex" \
		&& git -C "$output" commit  -m "[script]: add exo2.uncommitted.tex  ---  $(git status --short -- "exo2.uncommitted.tex")" >/dev/null

	echo "faite."
}
ex3check () {
	test -z "$(git diff sujet -- "exo3.tex")" \
		&& warning "Exercice 3 non fait (aucun changement détecté sur le fichier 'exo3.tex')..." \
		&& return 0

	echo "Vérification de l'exercice 3... "
	indent='  '

	#Test compilation
	cp "exo3.tex" -ft "$tmpdir/"
	pushd "$tmpdir" >/dev/null
	pdflatex -interaction nonstopmode "exo3.tex" >/dev/null 2>&1 </dev/null \
		|| warning "Le fichier 'exo3.tex' ne compile pas correctement."
	popd >/dev/null

	#Test commit
	git status --short "exo3.tex" | read \
		&& warning "Le contenu actuel du fichier 'exo3.tex' ne semble pas avoir été pris en compte dans la dernière version (Ex3.Q5)." \
		&& cp -f "exo3.tex" "$output/exo3.uncommitted.tex" \
		&& git -C "$output" add "exo3.uncommitted.tex" \
		&& git -C "$output" commit  -m "[script]: add exo3.uncommitted.tex  ---  $(git status --short -- "exo3.uncommitted.tex")" >/dev/null

	echo "faite."
}
ex4check () {
	test -z "$(git diff sujet -- "exo4.tex")" \
		&& warning "Exercice 4 non fait (aucun changement détecté sur le fichier 'exo4.tex')..." \
		&& return 0

	echo "Vérification de l'exercice 4... "
	indent='  '

	#Test compilation
	cp "exo4.tex" -ft "$tmpdir/"
	pushd "$tmpdir" >/dev/null
	pdflatex -interaction nonstopmode "exo4.tex" >/dev/null 2>&1 </dev/null \
		|| warning "Le fichier 'exo4.tex' ne compile pas correctement."
	popd >/dev/null

	#Test commit
	git status --short "exo4.tex" | read \
		&& warning "Le contenu actuel du fichier 'exo4.tex' ne semble pas avoir été pris en compte dans la dernière version (Ex4.Q8)." \
		&& cp -f "exo4.tex" "$output/exo4.uncommitted.tex" \
		&& git -C "$output" add "exo4.uncommitted.tex" \
		&& git -C "$output" commit  -m "[script]: add exo4.uncommitted.tex  ---  $(git status --short -- "exo4.uncommitted.tex")" >/dev/null

	echo "faite."
}
ex5check () {
	test -z "$(git diff sujet -- "exo5.tex")" \
		&& warning "Exercice 5 non fait (aucun changement détecté sur le fichier 'exo5.tex')..." \
		&& return 0

	echo "Vérification de l'exercice 5... "
	indent='  '

	#Test compilation
	cp "exo5.tex" -ft "$tmpdir/"
	pushd "$tmpdir" >/dev/null
	pdflatex -interaction nonstopmode "exo5.tex" >/dev/null 2>&1 </dev/null \
		|| warning "Le fichier 'exo5.tex' ne compile pas correctement."
	popd >/dev/null

	#Test commit
	git status --short "exo5.tex" | read \
		&& warning "Le contenu actuel du fichier 'exo5.tex' ne semble pas avoir été pris en compte dans la dernière version (Ex5.Q8)." \
		&& cp -f "exo5.tex" "$output/exo5.uncommitted.tex" \
		&& git -C "$output" add "exo5.uncommitted.tex" \
		&& git -C "$output" commit  -m "[script]: add exo5.uncommitted.tex  ---  $(git status --short -- "exo5.uncommitted.tex")" >/dev/null

	echo "faite."
}
ex6check () {
	test -z "$(git diff sujet -- "exo6.tex")" \
		&& warning "Exercice 6 non fait (aucun changement détecté sur le fichier 'exo6.tex')..." \
		&& return 0

	echo "Vérification de l'exercice 6... "
	indent='  '

	#Test exo6.tex réordonné
	if q2commit="$(git log --oneline --grep 'exo6 réordonné' | head -1 | cut -d' ' -f1)"
	then
		diff --brief <(git show sujet:exo6.tex | sort) <(git show $q2commit:exo6.tex | sort) >/dev/null \
			|| warning "La version réordonnée du fichier 'exo6.tex' (Ex6.Q1) n'a pas exactement les mêmes lignes que la version originale du sujet."
	else
		warning "Aucune version avec le message '$(set_font dim)exo6 réordonné$(set_font undim)' (Ex6.Q2)."
	fi

	#Test compilation
	cp "exo6.tex" -ft "$tmpdir/"
	pushd "$tmpdir" >/dev/null
	pdflatex -interaction nonstopmode "exo6.tex" >/dev/null 2>&1 </dev/null \
		|| warning "Le fichier 'exo6.tex' ne compile pas correctement."
	popd >/dev/null

	#Test lists
	grep -q '^[^%]*\\begin{enumerate}' "exo6.tex" \
		|| warning "Avez-vous bien fait une liste numérotée (Ex6.Q3)?"

	#Test référence
	grep -q '^[^%]*\\label' "exo6.tex" && grep -q '^[^%]*\\ref' "exo6.tex" \
		|| warning "Avez-vous bien utilisé le mécanisme de référence vu en cours (Ex6.Q4)?"

	#Test lists
	grep -q '^[^%]*\\begin{description}' "exo6.tex" \
		|| warning "Avez-vous bien fait une liste descriptive (Ex6.Q5)?"

	#Test lists
	grep -q '^[^%]*\\begin{itemize}' "exo6.tex" \
		|| warning "Avez-vous bien fait une liste non-numérotée (Ex6.Q6)?"

	#Test commit
	git status --short "exo6.tex" | read \
		&& warning "Le contenu actuel du fichier 'exo6.tex' ne semble pas avoir été pris en compte dans la dernière version (Ex6.Q7)." \
		&& cp -f "exo6.tex" "$output/exo6.uncommitted.tex" \
		&& git -C "$output" add "exo6.uncommitted.tex" \
		&& git -C "$output" commit  -m "[script]: add exo6.uncommitted.tex  ---  $(git status --short -- exo6.uncommitted.tex)" >/dev/null

	echo "faite."
}
ex7check () {
	test ! -f ".gitignore" \
		&& warning "Exercice 7 non-fait (aucun fichier '.gitignore' trouvé.)" \
		&& return 0

	echo "Vérification de l'exercice 7... "
	indent='  '

	#Test gitignore
	git status --ignored=no | grep -q '^*.\(pdf\|log\|aux\|toc\)$' \
		&& warning "Certains fichiers produits par la compilation LaTeX ne semble pas avoir été ignoré par git:  ils apparaissent dans la sortie de la commande $(set_font whitefg)git status$(set_font redfg).  Vérifiez le contenu de votre fichier '.gitignore'."
	#Test commit
	git status --short ".gitignore" | read \
		&& warning "Le contenu actuel du fichier '.gitignore' ne semble pas avoir été pris en compte dans la dernière version (Ex7.Q3)." \
		&& cp -f ".gitignore" "$output/.gitignore.uncommitted" \
		&& git -C "$output" add ".gitignore.uncommitted" \
		&& git -C "$output" commit  -m "[script]: add .gitignore.uncommitted  ---  $(git status --short -- ".gitignore.uncommitted")" >/dev/null

	echo "faite."
}

########### MAIN
#PARAMETERS
tmpdir="$HOME/.tmp-redacmathinfo"
output="$tmpdir/clone4archive"
#PARSE ARGS
mode="interactive"
exercices=""
while test $# -gt 0
do
	case "$1" in
		[1-7]|ex[1-7]|exo[1-7]|exo[1-7].*)
			exercices="$exercices $1";;
		--help|-h)
			echo "Usage: $0 [--help] [--yes|--no] [numéro d'exercice...]"
			echo
			echo "Plusieurs numéros d'exercice peuvent être donnés pour tester plusieurs exercices.  Sans argument, le script test tous les exercies.  Avec argument, le script ne teste que les exercices spécifiés.  Ceux-ci sont traités dans l'ordre et sans répétitions, quelque soit l'ordre des arguments.  L'option '--yes' répond \"oui\" si la question d'écraser une archive précédente se pose.  L'option '--no' indique de ne pas créer d'archive."
			echo
			echo "Exemple:  $0 -y 1 3 #test l'exerice 1 et l'exerice 3."
			exit 0;;
		--yes|--force|-y|-f)
			test "$mode" != "interactive" \
				&& error "Les options --yes et --no ne peuvent être utilisées ensemble." >&2 \
				&& exit 1
			mode='overwrite';;
		--no|-n)
			test "$mode" != "interactive" \
				&& error "Les options --yes et --no ne peuvent être utilisées ensemble." >&2 \
				&& exit 1
			mode='no-archive';;
		*)
	esac
	shift
done
test -z "$exercices" && exercices="1 2 3 4 5 6 7"

#CHECK
repocheck
#if succeeded, $output is a clone of . in which untracked files might be added

exercices="$(printf '%s\n' $exercices | sort | uniq)"
declare -i nexchecked=0
for x in $exercices
do
	nexchecked=$(($nexchecked + 1))
	case $x in
		1|exo1|exo1.*) ex1check;;
		2|exo2|exo2.*) ex2check;;
		3|exo3|exo3.*) ex3check;;
		4|exo4|exo4.*) ex4check;;
		5|exo5|exo5.*) ex5check;;
		6|exo6|exo6.*) ex6check;;
		7|exo7|exo7.*) ex7check;;
	esac
done

test "$mode" = "no-archive" \
	&& echo "$(set_font blue)Message: archive non générée.$(set_font)" \
	&& exit 0

test "$mode" = "overwrite" \
	&& _flag_f='-f'

#EXPORT
#touch "$output.tar"
git -C "$output" archive --format="tar" --output="$output.tar" HEAD
line="oui"
test -f "./rendezmoi.tar" -a -z "$_flag_f" \
	&& echo -n "Écraser le fichier './rendezmoi.tar' déjà présent (o/N) ?  " \
	&& read line
case "$line" in
	oui|Oui|OUI|yes|Yes|YES|o|O|y|Y)
		mv -f "$output.tar" "./rendezmoi.tar";;
	non|Non|NON|no|No|NO|n|N|'') 
		error "L'écrasement du fichier './rendezmoi.tar' a été refusé."
		exit 1;;
	*) error "Votre réponse '$line' à ma question n'a pas été comprise.  Votre fichier n'a pas été écrasé.  Votre archive n'a pas été créée."
		exit 2;;
esac

echo "$(set_font greenfg)=======================$(set_font)"
test "$nexchecked" -lt 7 \
	&& warning "Seuls les exercices testés ont été mis dans l'archive."  "Pour votre rendu final, pensez à exécutez le script sans arguments (sauf éventuellement -y)."
valid "Votre archive './rendezmoi.tar' a été créée avec succès.  Vous pouvez la déposer sur le dépôt correspondant à votre sur $(set_font bold)https://ent.uca.fr/moodle/course/view.php?id=26472$(set_font)$(set_font greenfg).  ⚠ N'avoir aucun avertissement n'est pas une garantie de réussite."

